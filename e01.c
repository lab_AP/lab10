#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "types.h"


int insert(node_t *root, int num, char *lastName, char *firstName, char *examination, int mark);
node_t * newNode();
node_t * search(int num, node_t *root);
exam_t * newExam();
//exam_t * insertExam(node_t *node, char *examination, int mark);

int main(int argc, char *argv[])
{
  printf("\n");
  if (argc != 2) {
    printf("Missing arg: %s input_file\n", argv[0]);
    exit(-1);
  }
  
  int i;
  char line[200];
  FILE *fp = fopen(argv[1], "r");
  node_t *root = newNode();

  if(fp == NULL){
    printf("fp is NULL\n");
    exit(-1);
  }
  if(root == NULL){
    printf("root is NULL\n");
    exit(-1);
  }
  int status;
  int num;
  char lastName[100];
  char firstName[100];
  char examination[100];
  int mark;
  
  while(fgets(line, 200-1, fp) != NULL) {
    sscanf(line, "%d %s %s %s %d", &num, lastName, firstName, examination, &mark);
    status = insert(root, num, lastName, firstName, examination, mark);
    if (status != 1) {
      printf("Error inserting the new value\n");
      exit(-1);
    }

  }
  return 0;
}

node_t * newNode()
{
    node_t *node = (node_t*)malloc(sizeof(node_t));
    if (node == NULL){
      printf("Fail allocating node_t\n");
      exit(-1);
    }
    node->left = NULL; node->right = NULL; node->exam = NULL;
    return node;
}

/*
 * Need a good insertion to make the pointer "next"  to point to something
 */
exam_t * newExam(char *name, int mark)
{
  exam_t *exam =(exam_t*)malloc(sizeof(exam_t));
  if (exam == NULL){
      printf("Fail allocating node_t\n");
      exit(-1);
    }
  strcpy(exam->exam_name, name);
  exam->grade = mark;
  exam->next = NULL;
  
  return exam;
}

int insert(node_t *root, int num, char *lastName, char *firstName, char *examination, int mark)
{
  node_t *node = search(num, root);
  if (node == NULL){
    node = newNode();
    node->number = num;
    strcpy(node->lastName, lastName);
    strcpy(node->firstName, firstName);
  }
  exam_t *exam  = newExam(examination, mark);
  exam_t *tmp = node->exam;
  if (tmp == NULL){
    node->exam = exam;
    return 1;
  }

  while (tmp->next != NULL)
    tmp=tmp->next;
  tmp->next = exam;

  return 1;
}

node_t * search(int num, node_t *root)
{
  if (root==NULL)
      return NULL;
  if(root->number == num)
    return root;
  if(root->left == NULL && root->right == NULL)
    return NULL;  
  
  if(root->number > num)
    return search(num, root->left);
  else if(root->number > num)
    return search(num, root->right);
}
