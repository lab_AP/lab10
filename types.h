#ifndef TYPES_PERSO
#define TYPES_PERSO

typedef struct exam{
  char exam_name[100];
  int grade;
  struct exam *next;
} exam_t;

typedef struct node{
  int number;
  char lastName[30];
  char firstName[30];
  exam_t *exam;
  struct node *left;
  struct node *right;
} node_t;

#endif
